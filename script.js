const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W    E    W       W W",
    "WW WW WWW WWW WWW W W",
    "W   W   W   W   W   W",
    "WWW WWW W W WWW WWW W",
    "W     W   W   W   W W",
    "W WWW WWWWW WWWWW WEW",
    "W W   W   W W     W W",
    "W WWWWW W W W WWWWW F",
    "S     W W W W     WWW",
    "WWWWW W W WWWWWWW   W",
    "W     W W   W   W W W",
    "W WWWWW W WWW W WWW W",
    "W      EW     W     W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

const container = document.getElementById("d0");

for(let rowIndex in map)
{
    const rowElement = document.createElement("div");
    rowElement.classList.add('row');
    container.appendChild(rowElement);

    for(let cellIndex in map[rowIndex])
    {
        const cellElement = document.createElement("div");
        cellElement.classList.add("cell");
        cellElement.id = rowIndex + "-" + cellIndex;

        function whatIsIt()
        {
            if(map[rowIndex][cellIndex] == "W")
                {cellElement.classList.add("wall");}

            else if(map[rowIndex][cellIndex] == " ")
                {cellElement.classList.add("floor");}
            else if(map[rowIndex][cellIndex] == "E")
                {cellElement.classList.add("night");}

            else if(map[rowIndex][cellIndex] == "S")
                {cellElement.classList.add("start");}

            else
                {cellElement.classList.add("exit");}            
        }
        whatIsIt();

        rowElement.appendChild(cellElement);
    }
}
const player = document.createElement("div");
player.classList.add("player");
document.getElementById("9-0").appendChild(player);


function logKey(e) 
{
    const parentLocation = player.parentElement.id;
    const currentLocation = parentLocation.split("-");    //currentLocation with ID
    
    if(!e.keyCode == "40" || !e.keyCode == "39" || !e.keyCode == "38" || !e.keyCode == "37")
    {return;}

    if(e.keyCode == "40")   //DOWN arrow
    {
        row = Number(currentLocation[0]) + 1;   
        cell = Number(currentLocation[1]) + 0;   
    }    
    if(e.keyCode == "38")    //UP arrow
    {
        row = Number(currentLocation[0]) - 1;
        cell = Number(currentLocation[1]) + 0;
    }
    if(e.keyCode == "37")   //LEFT arrow
    { 
        row = Number(currentLocation[0]) + 0;
        cell = Number(currentLocation[1]) - 1;
    }   
    if(e.keyCode == "39")   //RIGHT arrow
    {
        row = Number(currentLocation[0]) + 0;
        cell = Number(currentLocation[1]) + 1;
    }   
//=======================================================================================
    const nextPlayerMove = row + "-" + cell;
    const xxx = document.getElementById(nextPlayerMove);
       
    if(xxx.classList.contains("wall"))
        {return;}
    
    if(xxx.classList.contains("night"))
    {
        document.body.style.background = "url('./yYqAk9C.gif')";
        document.body.style.backgroundSize = "cover";
    }

    document.getElementById(nextPlayerMove).appendChild(player);

    if(xxx.classList.contains("exit"))
    {
        document.body.style.background = "url('./animated-forest-gif-6.gif')";
        document.body.style.backgroundSize = "cover";

        document.getElementById("winner").classList.add("winner");
    }

}

document.addEventListener('keydown', logKey);

